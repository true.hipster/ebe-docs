# EBE docs

API para facturación electronica.

Electronic Biller Engine Core es un API Stateless que abstrae la logica de comunicarse con el Ministerio de Hacienda de Costa Rica (MH), nace de la necesidad de centralizar la complejidad de la integración con el MH, además de centralizar los cambios de versión con sus implicaciones de posibles cambios sobre estructuras, metodos y algoritmos requeridos para dicha integración con el MH.

EBE-CORE es un servicio en la nube para disposición y uso de cualquier desarrollador, esta disponible la versión Stage que se mantendrá libre de uso para desarrollo y pruebas, y la versión Production que requiere un key de autenticación para su consumo.

Al ser un API Stateless el mismo actua como un proxy contra los servicios del MH, esto significa que no hay almacenamiento de datos de ningún tipo, ni en la recepción o respuesta de hacienda, el API es transparante y actual como “puente“ entre las integraciones y el MH; y su único fin es facilitar la integracion con el sistema de facturación electronica suministrado por el MH.

El API se compone de servicios los cuales se clasifican en Operativos y Utilitarios.

## Ambientes
|Recurso | Descripción |
| ------------- | ------------- |
| https://gateway.dev.factura-e.cr/catalog/api/  | Desarrollo / Pruebas  |
| https://gateway.factura-e.cr/catalog/api/ | Producción  |

## Operativos
Los servicios operatvios abstraen toda la logica necesaria de enviar y consultar un documento fiscal desde/hacia el MH, encapsulan toda la logica en un simple llamado REST, dichos servicios son los siguientes:

- Enviar un documento hacia el MH
- Consultar el estado del documento por el consecutivo suministrado por el MH (/reception/{numeroComprobante}).

|Recurso | Verbo |Descripción|
| ------------- | ------------- |-------------|
| https://gateway.dev.factura-e.cr/invoice/api/invoice/in-memory/process | POST |Enviar documento |
| https://gateway.dev.factura-e.cr/invoice/api/reception/{consecutivo}| GET  |Obtener documento|

## Enviar documento

`POST /invoice/api/invoice/in-memory/process
Content-Type: application/json
X-Username: cpj-XXXXXXXXX@stag.comprobanteselectronicos.go.cr
X-Password: XXXXXXXXXXXX`

```

{
    "keyStoreType":"PKCS12",
    "keyStore":"Base 64 p12",
    "keyStorePassword":"0000 (PIN)",
    "key": "*** XXX (50 caracteres) ***",
    "economicActivity": "*** XXX ***",
    "consecutiveNumber": "*** XXX ***",
    "issuedDate": "2019-06-12T20:30:59-06:00",
    "transmitter": {
      "name": "*** XXX ***",
      "identification": {
        "type": "01",
        "number": "*** XXX ***"
      },
      "location": {
        "province": "1",
        "canton": "01",
        "district": "01",
        "neighborhood": "04",
        "exactDirection": "*** XXX ***"
      },
      "phone": {
        "countryCode": "506",
        "number": "*** XXX ***"
      },
      "fax": {
        "countryCode": "506",
        "number": "*** XXX ***"
      },
      "email": "*** XXX ***"
    },
    "receiver": {
      "name": "*** XXX ***",
      "identification": {
        "type": "01",
        "number": "*** XXX ***"
      }
    },
    "saleCondition": "01",
    "creditTerm":"*** XXX ***",
    "paymentMethod": ["01"],
    "detail": {
      "line": [{
        "lineNumber": "1",
        "code":"01",
        "commercialCode": [{
          "type": "04",
          "code": "*** XXX ***"
        },
        {
          "type": "04",
          "code": "*** XXX ***"
        }],
        "quantity": "*** XXX ***",
        "unitMeasurement": "Unid",
        "unitMeasurementCommercial": "Unid",
        "detail": "*** XXX ***",
        "unitPrice": "*** XXX ***",
        "totalAmount": "*** XXX ***",
        "discount": [{
          "discountAmount": "0.00000",
          "discountOrigin": "discountOrigin"
        },
        {
          "discountAmount": "0.00000",
          "discountOrigin": "discountOrigin 2"
        }],
        "subTotal": "*** XXX ***",
        "taxableBase":"0.00000",
        "taxNet": "0.00000",
        "lineTotalAmount": "*** XXX ***"
      }]
    },
    "invoiceSummary": {
        "currencyType": {
          "currencyCode": "CRC",
          "exchangeRate": "585.69000"
        },
      "totalTaxableServices": "0.00000",
      "totalExemptServices": "0.00000",
      "totalExoneratedServices":"0.00000",
      "totalTaxedGoods": "0.00000",
      "totalExemptGoods": "0.00000",
      "totalExoneratedGoods":"0.00000",
      "totalTaxed": "0.00000",
      "totalExempt": "0.00000",
      "totalExonerated":"0.00000",
      "totalSales": "0.00000",
      "TotalDescuentos": "0.00000",
      "TotalVentaNeta": "0.00000",
      "totalDiscount": "0.00000",
      "netSale": "0.00000",
      "totalTax":"0.00000",
      "totalReturnedIva":"0.00000",
      "totalOtherCharges":"0.00000",
      "totalVoucher": "0.00000"
    },
    "referenceInformation": [{
        "documentType":"01",
        "number": "0123456789",
        "issuedDate":"2018-10-10T09:00:00-06:00",
        "code":"05",
        "reasonReference":"*** XXX ***"
    }],
    "others": { "otherText": "*** XXX ***" }
  }

```

## Consultar documento

`GET /invoice/api/reception/{numeroConsecutivo}
X-Username: cpj-XXXXXXXXX@stag.comprobanteselectronicos.go.cr
X-Password: XXXXXXXXXXXX`

```

{
    "date": "2019-06-04T00:00:00-06:00",
    "status": "aceptado",
    "xml": "Base 64  xml",
    "key": "{numeroConsecutivo}",
    "transmitterName": "Nombre Emisor",
    "transmitterIdentificationType": "** XX **",
    "transmitterIdentificationNumber": "** XX **",
    "receiverName": "Nombre Receptor",
    "receiverIdentificationType": "** XX **",
    "receiverIdentificationNumber": "** XX **",
    "message": "1",
    "messageDetail": "Este comprobante fue aceptado en el ambiente de pruebas, por lo cual no tiene validez para fines tributarios",
    "totalTaxAmount": 0,
    "totalAmountInvoice": 1900
}

```

Además de poder consumir estos servicios operativos “completos”, se pueden consumir las partes que lo componen de forma individual, que son los siguientes servicios:

- Firmar un documento fiscal. (/in-memory/sign-document)
- Firmar y validar un documento fiscal (mismo proceso, solo que `NO` envia al MH).
- Validar credenciales (/validate/credentials).
- Obtener la informacion de un cetificado (/certificate-information).

|Recurso | Verbo |Descripción|
| ------------- | ------------- |-------------|
| https://gateway.dev.factura-e.cr/dss/api/in-memory/sign-document| POST |Firmar documento |
| https://gateway.dev.factura-e.cr/dss/api/certificate-information| POST  |Obtener información del certificado|
| https://gateway.dev.factura-e.cr/invoice/api/validate/credentials| POST  |Validar credenciales de hacienda|

## Utilitarios
Los servicios utilitarios permiten consumir una variedad de funcionalidades las cuales suelen ser muy útiles a la hora de trabajar con documentos fiscales con el MH; algunos de estos servicios son los siguientes:

- Tipo de cambio (obtenido diariamente desde el Banco Central de Costa Rica). 
- Distribucion territorial de Costa Rica (Catalogo de Provincias, cantones y distritos). `Pendiente de liberar`
- Consultas al patron electoral por cedula fisica nacional.
- Consultas a catalogos tipo “ENUMS“ definidos por el MH como:
    - Tipo de identificación
    - Actividades economicas `Pendiente de liberar`
    - Tipos de documentos fiscales `Pendiente de liberar`
    - Otros

|Recurso | Verbo |Descripción|
| ------------- | ------------- |-------------|
| https://gateway.dev.factura-e.cr/catalog/api/exchange-request-current/318| GET |Obtener tipo de cambio |
| https://gateway.dev.factura-e.cr/catalog/api/identificationTypeList| GET  | Obtener catalogo de identificacion|
| https://gateway.dev.factura-e.cr/catalog/api/voter-user-information/{numeroCedula}| GET  | Obtener informacion por cedula fisica nacional|

## Postman Collection
Aca esta la colección en Postman para su uso, https://www.getpostman.com/collections/2f073a796fa4fd68a9f5.

## Convertidor en linea de .p12 a Base 64
https://www.browserling.com/tools/file-to-base64